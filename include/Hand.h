#ifndef HAND_H
#define HAND_H

#include <vector>
#include <string>
#include "Deck.h"
#include "Card.h"

class Hand
{
private:
	int maxCards;
	std::vector<Card> CardHand;
	bool ShuffleTrue;
public:
	Hand();
	Hand(int);
	Hand(Deck&, int);
	void Draw(Deck&);
	void Discard(Deck&, int);
	void SETmaxCards(int);
	int GETmaxCards();
	std::string at(int);

    int FindString(std::string);
	int size();
	bool contains(std::string);
	void sort(std::vector<Card>& CardHand, int, int);
	int partition(std::vector<Card>& CardHand, int, int);
	void swap(Card&, Card&);

};

#endif
