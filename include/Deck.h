#ifndef DECK_H
#define DECK_H
#include "Card.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

class Deck
{
    public:
    Deck();
    //virtual ~Deck();
    Deck(int);
    void ToDiscard(Card&);
    int getnumcard();
    void ReShuffle();
    private:
    std::vector<Card> InDeck, InDiscard;
    int numcard;
    int type;

};

#endif // DECK_H
