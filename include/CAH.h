#pragma once

#include "Player.h"
#include "Deck.h"
#include <vector>
#include <string>

class CAH {
	private:
		//Declare game variables
		int numPlayers;
		int numHumans;
		std::vector<Player> Group;
        Deck BlackDeck; //= new Deck(2);
		Deck WhiteDeck;//= new Deck(3);

		//Declare game functions
		void Setup();
		bool Run();
		void Win();
		bool GameOver();
		void TearDown();
	public:
		CAH();
		std::wstring Title;
<<<<<<< HEAD
        const static int DrawFrom = 4;
=======
        const int DrawFrom = 3;
>>>>>>> ab8f7492419efd4baf453d5d5ab28045bb1575b5
		void createPlayers();
		int GETnumPlayers();
		int GETnumHumans();
		void SETnumPlayers(int);
		void SETnumHumans(int);
		void SETtitle(std::wstring);
		Player PlayerAt(int);
};
