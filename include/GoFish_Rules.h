#pragma once
#include "Rules.h"
class GoFish_Rules:public Rules
{
	public:
		GoFish_Rules();
		~GoFish_Rules();
		void turn();
		void book();
		void GoFish();
		bool GoFish_Rules::deckZero();
		bool GoFish_Rules::handZero();

	private: 
		std::vector<Hand> playerCards;
		std::vector<Deck> gameDeck ;

};

