#ifndef LOAD_H
#define LOAD_H

#include <string>
#include <fstream>
#include <cstdio>
#include "CrazyEights.h"
#include "GoFish.h"
#include "CAH.h"
#include "OldMaid.h"
#include "Sevens.h"

class Load
{
private:
    std::string data;
    std::ifstream fin;

    void decode();
	void decode(std::wstring&);
	bool LoadfromFile(std::string);
	bool AskLoad();
	bool dataExists();
public:
	Load();
	Load(CrazyEights*);
	Load(GoFish*);
	Load(CAH*);
	Load(OldMaid*);
	Load(Sevens*);
	std::wstring LoadTitle(std::wstring);
	void decode2(std::wstring&);
};

#endif
