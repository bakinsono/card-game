#ifndef SAVE_H
#define SAVE_H

#include <string>
#include <fstream>
#include "CrazyEights.h"
#include "GoFish.h"
#include "CAH.h"
#include "OldMaid.h"
#include "Sevens.h"

class Save
{
private:
    std::string data;
    std::ofstream fout;
    void encode();
	void SavetoFile(std::string, std::string);
	std::string to_string(int i);
public:
	Save(CrazyEights*);
	Save(GoFish*);
	Save(CAH*);
	Save(OldMaid*);
	Save(Sevens*);
};

#endif
