#pragma once

#include "Player.h"
#include "Deck.h"
#include <vector>
#include <string>

class GoFish
{
	private:
		//Declare game variables
		int numPlayers;
		int numHumans;
		std::vector<Player> Group;
		Deck DefaultDeck;

		//Declare game functions
		void Setup();
		bool Run();
		void Win();
		bool GameOver();
		void TearDown();
	
	public:
		GoFish();
		std::wstring Title;
        const int DrawFrom = 0;
		void createPlayers();
		int GETnumPlayers();
		int GETnumHumans();
		void SETnumPlayers(int);
		void SETnumHumans(int);
		void SETtitle(std::wstring);
		Player PlayerAt(int);
};