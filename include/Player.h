#ifndef PLAYER_H
#define PLAYER_H

#include "Hand.h"
#include "Deck.h"

class Player
{
private:
	int number;
	int Score;
	int Type;
public:
	Player();
	Player(Deck&, int=5, int=1, int=0);
	Hand HAND;
	int numPlayers(int);
	
	int getScore();
	int getHandSize();
	int getType();
};

#endif
