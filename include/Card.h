#ifndef CARD_H
#define CARD_H

#include <string>
#include <vector>

class Card
{
private:
	std::string message;
	int value;
	int suit;
	int pos;
public:
	Card();
	Card(int, int, std::string, int);
	std::string Face();
	int getPosition ();
	int getValue();
	int getSuit();
	Card& operator=(Card);
	bool operator!=(std::string);
	int operator[](int);
	Card& operator+(Card&);
	Card& operator+=(Card& );
	bool operator<(Card& );
	bool operator>(Card& );
	bool operator==(Card& );


};
#endif
