#ifndef DISPLAY_H
#define DISPLAY_H

#include <string>

class Display
{
public:
	Display();
	bool AskToLoad();
	void ShowTitle(std::wstring);
	void clear();
};

#endif