var searchData=
[
  ['uninitialize',['uninitialize',['../struct_cpp_unit_test_plug_in.html#a8628d2026e76c58f715e17af88f77458',1,'CppUnitTestPlugIn']]],
  ['unload',['unload',['../class_plug_in_manager.html#a5366b1e0e2e96e84d09e26893c00c4bf',1,'PlugInManager::unload(const std::string &amp;libraryFileName)'],['../class_plug_in_manager.html#afc1fa045afaac73cd44f69839056f1e1',1,'PlugInManager::unload(PlugInInfo &amp;plugIn)']]],
  ['unregisterfactory',['unregisterFactory',['../class_test_factory_registry.html#afa3fb925b07eb34e9ccfab84812afc18',1,'TestFactoryRegistry']]],
  ['up',['up',['../class_test_path.html#a396e9d13b59f5e534582e80e3f1a2c42',1,'TestPath']]]
];
