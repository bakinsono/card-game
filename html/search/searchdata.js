var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuwx~",
  1: "abcdefghlmopqrstwx",
  2: "hst",
  3: "abcdefghilmnopqrstuwx~",
  4: "pt",
  5: "c",
  6: "ls",
  7: "c",
  8: "w",
  9: "drt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "typedefs",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Typedefs",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

