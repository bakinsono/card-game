#include <string>
#include <vector>
#include <iostream>
#include "../include/Card.h"

Card::Card()
{}

//Constructor
Card::Card(int p, int s, std::string m, int v)
{
    int pos = p;
    int suit = s;
    std::string message = m;
    int value = v;
}

//Setters and getters
std::string Card::Face()
{
    return message;
}

//Written by: Oyinkan
int Card::getPosition()
{
	return pos;
}

//Written by: Oyinkan
int Card::getSuit()
{
	return suit;
}

//Written by: Oyinkan
int Card::getValue()
{
	return value;
}


//Written By: Evan
Card& Card::operator=(Card RIGHT)
{
	this->message = RIGHT.Face();
	return *this;
}

//Written By: Evan
bool Card::operator!=(std::string message)
{
	return this->message == message;
}


int Card::operator[](int X)
{
	return pos;
}

//Operators
//Written by: Oyinkan
Card& Card::operator+(Card& c)
{
	Card t(*this);
	t.value += c.value;
	//I commented this out so the compiler will work. if you want to use += then you also have to write a += operator
	return t;
}

/*Broken so commented out
Card& operator+=(Card c)
{
	Card temp;
	 += c.value;
	return *this;
}
*/

bool Card::operator<(Card& c)
{
	return value < c.value;
}

bool Card::operator>(Card& c)
{
	return !(value < c.value);
}

bool Card::operator==(Card& c)
{
	return ( (value == c.value) && (suit == c.suit) );
}

