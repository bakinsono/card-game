#include <string>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <sstream>
#include <cmath>
#include "../include/Save.h"

using namespace std;

///Purpose:
///Written By: Evan
Save::Save(CrazyEights* X)
{
	SavetoFile("CrazyEights", "CrazyEightsUnencrypted");
}

///Purpose:
///Written By: Evan
Save::Save(GoFish* X)
{
	SavetoFile("GoFish", "GoFishUnencrypted");
}

///Purpose:
///Written By: Evan
Save::Save(CAH* X)
{
	//Sets the text that will be outputted to the save file
	data.append("[int] ;");

	//Each variable in the int category will be stored like this.
/*	data.append("Number Players:" + to_string(X->GETnumPlayers()) + ";");
	data.append("Number Humans:" + to_string(X->GETnumHumans()) + ";");

	data.append("[Player] ;");
	for (int n = 0; n < X->GETnumPlayers(); n++)
	{
		data.append("Player " + to_string(n + 1));
        data.append("{ Type: " + to_string(X->PlayerAt(n).getType()));
        data.append(" Score: " + to_string(X->PlayerAt(n).getScore()));
        data.append(" Hand: [");
		for (int k = 0; k < X->PlayerAt(n).HAND.size(); k++)
		{
			data.append(to_string(999) + ", ");
		}
			data.append("]};");
	}
*/
	SavetoFile("CardsAgainstHumanity", "CAHUnencrypted");
}

///Purpose:
///Written By: Evan
Save::Save(OldMaid* X)
{
	SavetoFile("OldMaid", "OldMaidUnencrypted");
}

///Purpose:
///Written By: Evan
Save::Save(Sevens* X)
{
	SavetoFile("Sevens", "SevensUnencrypted");
}

///Purpose:
///Written By: Evan
void Save::encode()
{
	//encodes the data before saving
	for (unsigned int i = 0; i < data.length(); i++) {
		srand(i);
		data[i] = data[i] + floor((sqrt(i + rand() % 100) + 1));
	}
}

///Purpose:
///Written By: Evan
void Save::SavetoFile(string real, string forTesting)
{
	//Replaces all semicolons with a newline character for readability in the file
	replace(data.begin(), data.end(), ';', '\n');

	fout.open(("../saves/Save Text Files/"+forTesting+".txt").c_str());
	fout << data;
	fout.close();

	encode();

	//save
	fout.open(("../saves/"+real+".sav").c_str());
	fout << data;
	fout.close();
}

string to_string(int i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}
