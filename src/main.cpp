/**

write comments here





Written By:
Evan Svendsen 001186167
Oyinkan Bakinson		student #
Alexander Baruta		student #
Shohei Saito		student #
*/

#include <iostream>

#include "../include/CAH.h"
#include "../include/CrazyEights.h"
#include "../include/GoFish.h"
#include "../include/OldMaid.h"
#include "../include/Sevens.h"
#include "../include/Display.h"
#include "../include/Load.h"

using namespace std;

int main()
{
	//wstring Title = Load().LoadTitle(L"MainMenu");
	//Load().decode2(Title);
	//Display().clear();
	//Display().ShowTitle(Title);

	cout<<"1. Crazy Eights"<<endl
		<<"2. Go Fish"<<endl
		<<"3. Cards Against Humanity"<<endl
		<<"4. Old Maid"<<endl
		<<"5. 7's"<<endl<<endl
		<<">> ";

	int game;
	cin >> game;

	switch (game) {
	case 1:
		CrazyEights();
		break;
	case 2:
		GoFish();
		break;
	case 3:
		CAH();
		break;
	case 4:
		OldMaid();
		break;
	case 5:
		Sevens();
		break;
	}

	return 0;
}
