﻿#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>
#include <stdlib.h>
#include "../include/Display.h"

using namespace std;

Display::Display()
{
}

//Written By: Evan
bool Display::AskToLoad()
{
	//TO DO: Saito can you please write this code
	return false;
}

//Written By: Evan
void Display::ShowTitle(wstring Title)
{
	//Sets the display window font to Unicode
	//_setmode(_fileno(stdout), 0x00020000);

	//Output the title to the screen (must be a wide string)
	wcout <<Title<<endl;

	//Returns the window font back to standard text
	//_setmode(_fileno(stdout), _O_TEXT);
	fflush(stdout);
}


//Written By: Evan
void Display::clear()
{
	//Remove everything else from the screen
	system("cls");
	//Resize the terminal window to fit the title that needs to be displayed
	system("MODE CON COLS=200 LINES=40");
	cout << endl << endl;
}
