#include <iostream>
#include "../include/Player.h"
#include "../include/Deck.h"

using namespace std;

Player::Player() { Score = 0; }

//Constructor: Player sitting next, Draw From Deck, Maximum Cards, Player Turn Number, Player Type (0 = Human, 1 = AI)
//Written By: Evan
Player::Player(Deck& DrawFrom, int HandSize, int num, int PlayerType)
{
	HAND.SETmaxCards(HandSize);
	for(int i=0; i<HandSize; i++)
		HAND.Draw(DrawFrom);
	number = num;
	Score = 0;
	Type = PlayerType;
}

//Written By: Evan
int Player::numPlayers(int max)
{
	int num = 0;
	//Ask the user for the number of players (2-max)
	while (num < 2 || num > max)
	{
		cout <<endl<<endl<<endl<< "Please indicate the number of players (2-"<<max<<"): ";
		cin >> num;
	}
	return num;
}

int Player::getScore() { return 0; }
int Player::getHandSize() { return HAND.GETmaxCards(); }
int Player::getType() { return Type; }