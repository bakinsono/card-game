/*
.----------------.  .----------------.   .----------------.  .----------------.  .----------------.  .----------------.
| .--------------. || .--------------. | | .--------------. || .--------------. || .--------------. || .--------------. |
| |    ______    | || |     ____     | | | |  _________   | || |     _____    | || |    _______   | || |  ____  ____  | |
| |  .' ___  |   | || |   .'    `.   | | | | |_   ___  |  | || |    |_   _|   | || |   /  ___  |  | || | |_   ||   _| | |
| | / .'   \_|   | || |  /  .--.  \  | | | |   | |_  \_|  | || |      | |     | || |  |  (__ \_|  | || |   | |__| |   | |
| | | |    ____  | || |  | |    | |  | | | |   |  _|      | || |      | |     | || |   '.___`-.   | || |   |  __  |   | |
| | \ `.___]  _| | || |  \  `--'  /  | | | |  _| |_       | || |     _| |_    | || |  |`\____) |  | || |  _| |  | |_  | |
| |  `._____.'   | || |   `.____.'   | | | | |_____|      | || |    |_____|   | || |  |_______.'  | || | |____||____| | |
| |              | || |              | | | |              | || |              | || |              | || |              | |
| '--------------' || '--------------' | | '--------------' || '--------------' || '--------------' || '--------------' |
'----------------'  '----------------'   '----------------'  '----------------'  '----------------'  '----------------'

*/

#include <iostream>
#include <vector>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "../include/GoFish.h"
#include "../include/Deck.h"
#include "../include/Player.h"
#include "../include/Card.h"
#include "../include/Hand.h"
#include "../include/Save.h"
#include "../include/Load.h"
#include "../include/Display.h"

using namespace std;

GoFish::GoFish()
{
	do 
	{
		TearDown();
		Setup();
	} 
	while (Run());

	TearDown();
}


void GoFish::Setup()
{
	Load(this);
	Display().clear();
	Display().ShowTitle(Title);

	//Maximum Players: 10
	numPlayers = Player().numPlayers(10);

	createPlayers();

	Save(this);
}

bool GoFish::Run()
{
	int currentplayer = 0;

	while (1)
	{
		Display().clear();
		Display().ShowTitle(Title);
		if (GameOver())
		{
			Win();
			break;
		}

		//For testing purposes only. To be removed later
		for (unsigned int i = 0; i < Group.size(); i++)
			for (int j = 0; j < 10; j++)
				cout << i + 1 << ": " << Group.at(i).HAND.at(j) << endl;







		//GAME LOGIC GOES HERE
		//RULE CALLS
		//PLAYER TURNS
		//END OF PLAYER TURN






		//Set the next player in the group circle
		currentplayer++;
		if (currentplayer > numPlayers - 1)
			currentplayer = 0;
	}

	//Ask to repeat the game
	char input;
	cout << "Play again (Y/N): ";
	cin >> input;
	if (input == 'Y' || input == 'y')
		return true;
	return false;
}

void GoFish::Win()
{
    
}


bool GoFish::GameOver()
{
//something like reseting, setting scores to player, setting up for new game
//win coddition met with points? cards? or count at the end?
	if (1 == 0)
		return true;
	for (int i = 0; i<numPlayers - 1; i++)
		if (0 == 1)//.at(i).getScore() == -numPlayers + 14)
			return true;
	return false;
}

void GoFish::TearDown()
{
	Group.clear();
}

//Edited by: Oyinkan
void GoFish::createPlayers()
{
	Player* temp;
	//Create all remaining players
	for (int k = 0; k < numPlayers; k++)
	{
		if (numPlayer > 4)
		{
			//Deck drawn from, number of cards drawn, player number, 0 indicates human
			temp = new Player(DefaultDeck, 5, k + 1, 0);
			Group.push_back(*temp);
			delete temp;
		}
		else
		{
			temp = new Player(DefaultDeck, 7, k + 1, 0);
			Group.push_back(*temp);
			delete temp;
		}
	}
	temp = NULL;
}

//Setters and Getters
int GoFish::GETnumPlayers() { return numPlayers; }
int GoFish::GETnumHumans() { return numHumans; }
void GoFish::SETnumPlayers(int a) { numPlayers = a; }
void GoFish::SETnumHumans(int a) { numHumans = a; }
void GoFish::SETtitle(wstring x) { Title.erase(); Title.append(x); }
Player GoFish::PlayerAt(int z) { return Group[z]; }

