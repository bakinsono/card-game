﻿/*

 ██████╗ █████╗ ██████╗ ██████╗ ███████╗     █████╗  ██████╗  █████╗ ██╗███╗   ██╗███████╗████████╗    ██╗  ██╗██╗   ██╗███╗   ███╗ █████╗ ███╗   ██╗██╗████████╗██╗   ██╗
██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔════╝    ██╔══██╗██╔════╝ ██╔══██╗██║████╗  ██║██╔════╝╚══██╔══╝    ██║  ██║██║   ██║████╗ ████║██╔══██╗████╗  ██║██║╚══██╔══╝╚██╗ ██╔╝
██║     ███████║██████╔╝██║  ██║███████╗    ███████║██║  ███╗███████║██║██╔██╗ ██║███████╗   ██║       ███████║██║   ██║██╔████╔██║███████║██╔██╗ ██║██║   ██║    ╚████╔╝
██║     ██╔══██║██╔══██╗██║  ██║╚════██║    ██╔══██║██║   ██║██╔══██║██║██║╚██╗██║╚════██║   ██║       ██╔══██║██║   ██║██║╚██╔╝██║██╔══██║██║╚██╗██║██║   ██║     ╚██╔╝
╚██████╗██║  ██║██║  ██║██████╔╝███████║    ██║  ██║╚██████╔╝██║  ██║██║██║ ╚████║███████║   ██║       ██║  ██║╚██████╔╝██║ ╚═╝ ██║██║  ██║██║ ╚████║██║   ██║      ██║
 ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝       ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝      ╚═╝

*/

#include <iostream>
#include <vector>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "../include/CAH.h"
#include "../include/Deck.h"
#include "../include/Player.h"
#include "../include/Card.h"
#include "../include/Hand.h"
#include "../include/Save.h"
#include "../include/Load.h"
#include "../include/Display.h"

using namespace std;

//Written By: Evan
CAH::CAH() {
	//Run the game
	do {
		TearDown();
		Setup();
	} while (Run());

	TearDown();
}

//Written By: Evan
void CAH::Setup()
{
	Load(this);
	Display().clear();
	Display().ShowTitle(Title);

	//Maximum Players: 8
	numPlayers = Player().numPlayers(8);

	createPlayers();

	Save(this);
}

//Written By: Evan
bool CAH::Run()
{
	int currentplayer = 0;

	while (1)
	{
		Display().clear();
		Display().ShowTitle(Title);
		if (GameOver())
		{
			Win();
			break;
		}

		//For testing purposes only. To be removed later
		for (unsigned int i = 0; i < Group.size(); i++)
			for (int j = 0; j < 10; j++)
				cout << i + 1 << ": " << Group.at(i).HAND.at(j) << endl;







		//GAME LOGIC GOES HERE
		//RULE CALLS
		//PLAYER TURNS
		//END OF PLAYER TURN






		//Set the next player in the group circle
		currentplayer++;
		if (currentplayer > numPlayers - 1)
			currentplayer = 0;
	}

	//Ask to repeat the game
	char input;
	cout << "Play again (Y/N): ";
	cin >> input;
	if (input == 'Y' || input == 'y')
		return true;
	return false;
}

//Written By: Evan
void CAH::Win()
{
    
}

//Written By: Evan
bool CAH::GameOver()
{
//something like reseting, setting scores to player, setting up for new game
//win coddition met with points? cards? or count at the end?
	if (1 == 0)
		return true;
	for (int i = 0; i<numPlayers - 1; i++)
		if (0 == 1)//.at(i).getScore() == -numPlayers + 14)
			return true;
	return false;
}

void CAH::TearDown()
{
	Group.clear();
}

void CAH::createPlayers()
{
	Player* temp;
	//Create all remaining players
	for (int k = 0; k < numPlayers; k++)
	{
		//Deck drawn from, number of cards drawn, player number, 0 indicates human
		temp = new Player(WhiteDeck, 10, k + 1, 0);
		Group.push_back(*temp);
		delete temp;
	}
	temp = NULL;
}

//Setters and Getters
int CAH::GETnumPlayers() { return numPlayers; }
int CAH::GETnumHumans() { return numHumans; }
void CAH::SETnumPlayers(int a) { numPlayers = a; }
void CAH::SETnumHumans(int a) { numHumans = a; }
void CAH::SETtitle(wstring x) { Title.erase(); Title.append(x); }
Player CAH::PlayerAt(int z) { return Group[z]; }
