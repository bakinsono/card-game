#include "Deck.h"
#include "Card.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

Deck::Deck(){}

Deck::Deck(int dnum)
{
 //variable declaration
    int pos=0;
    int sint;
    int vint;

    string raw, name, suit, value;
    ifstream DataIn;

    //Open the data file
    //Since there are 4 different decks that we are using we use a flag to read the correct one in from file

    if (dnum==0)
    {
        DataIn.open("/data/DefaultDeck.txt");
        //Reading in the entire file line by line
    }
    if (dnum==1)
    {
        DataIn.open("/data/Default_Joker.txt");
    }
    if (dnum==2)
    {
        DataIn.open("/data/BlackDeck.txt");
    }
    if (dnum==3)
    {
        DataIn.open("/data/WhiteDeck.txt");
    }

    while(getline(DataIn, raw))
    {
        //Finding the first substring
        pos = raw.find("&");
        //Checking for the existance of the character
        if(pos!=0)
        {
            //Taking the "Name" substring
            name=raw.substr(0,pos);

        }
        //Removing the "Name" substring from the raw data
        raw=raw.substr(pos+1,string::npos);
        //Finding the second substring
        pos = raw.find("&");
        //Checking for the existance of the second substring
        if(pos!=0)
        {
            //Extracting the "Suit" substring
            suit=raw.substr(0, pos);
            //Extracting the "Value" substring
            value=raw.substr(pos+1, string::npos);

            //Converting the suit and value into int form to pass to the constructor
            stringstream vconvert(value);
            vconvert>>vint;
            stringstream sconvert(suit);
            sconvert>>sint;

            //Creating a new Card with the value and then pushing it onto the deck
            //InDeck.emplace(numcard, sint, name, vint);
            numcard++;
        }
    }
}

//Deck::~Deck()
//{
  //  return;
//}

void Deck::ReShuffle()
{
    while (InDiscard.size() != 0)
    {
        //Moves the cards from the discard to the draw deck
        InDeck.push_back(InDiscard.back());
        InDiscard.pop_back();
    }
}

void Deck::ToDiscard(Card& O)
{
InDiscard.push_back(O);
}

int Deck::getnumcard()
{
    return InDeck.size();
}
