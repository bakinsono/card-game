﻿#include <string>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <cwchar>

#include "../include/Load.h"
#include "../include/Display.h"

using namespace std;

//Default Constructor
Load::Load() {}

///Purpose:
///Written By: Evan
Load::Load(CrazyEights* Y)
{
	wstring Title(L"TitleCrazyEights");
	Title = LoadTitle(Title);
	decode(Title);
	//Y->SETtitle(Title);

	if (!LoadfromFile("CrazyEights"))
		return;
	if (!AskLoad())
		return;
	decode();

	int start, end;

	start = data.find("Number Players:") + 15;
	end = data.find("\n", start);
	//Y->SETnumPlayers(atoi((data.substr(start, end)).c_str()));

	start = data.find("Number Humans:") + 14;
	end = data.find("\n", start);
	//Y->SETnumHumans(atoi((data.substr(start, end)).c_str()));
}

///Purpose:
///Written By: Evan
Load::Load(GoFish* Y)
{
	wstring Title(L"TitleGoFish");
	Title = LoadTitle(Title);
	decode(Title);
	//Y->SETtitle(Title);

	if (!LoadfromFile("GoFish"))
		return;
	if (!AskLoad())
		return;
	decode();

	int start, end;

	start = data.find("Number Players:") + 15;
	end = data.find("\n", start);
	//Y->SETnumPlayers(atoi((data.substr(start, end)).c_str()));

	start = data.find("Number Humans:") + 14;
	end = data.find("\n", start);
	//Y->SETnumHumans(atoi((data.substr(start, end)).c_str()));
}

///Purpose:
///Written By: Evan
Load::Load(CAH* Y)
{
	//Load the game title
	wstring Title(L"TitleCAH");
	Title = LoadTitle(Title);
	decode(Title); //The title loaded is encrypted, so it needs to be decoded before outputting to the console.
	Y->SETtitle(Title);

	//Load the game data. Returns false if the file is empty or non existant.
	if (!LoadfromFile("CardsAgainstHumanity"))
		return;
	//Gives the player the option to load or start a new game
	if (!AskLoad())
		return;
	decode();

	//All the variables below have to be converted into their proper type. ie. string -> int
	//And then use the set functions of the game to override the default variabels

	int start, end;

	start = data.find("Number Players:") + 15;
	end = data.find("\n", start);
	Y->SETnumPlayers(atoi((data.substr(start, end)).c_str()));

	start = data.find("Number Humans:") + 14;
	end = data.find("\n", start);
	Y->SETnumHumans(atoi((data.substr(start, end)).c_str()));
}

///Purpose:
///Written By: Evan
Load::Load(OldMaid* Y)
{
	wstring Title(L"TitleOldMaid");
	Title = LoadTitle(Title);
	decode2(Title);
	//Y->SETtitle(Title);

	if (!LoadfromFile("OldMaid"))
		return;
	if (!AskLoad())
		return;
	decode();

	int start, end;

	start = data.find("Number Players:") + 15;
	end = data.find("\n", start);
	//Y->SETnumPlayers(atoi((data.substr(start, end)).c_str()));

	start = data.find("Number Humans:") + 14;
	end = data.find("\n", start);
	//Y->SETnumHumans(atoi((data.substr(start, end)).c_str()));
}

///Purpose:
///Written By: Evan
Load::Load(Sevens* Y)
{
	wstring Title(L"TitleSevens");
	Title = LoadTitle(Title);
	decode2(Title);
	//Y->SETtitle(Title);

	if (!LoadfromFile("Sevens"))
		return;
	if (!AskLoad())
		return;
	decode();

	int start, end;

	start = data.find("Number Players:") + 15;
	end = data.find("\n", start);
	//Y->SETnumPlayers(atoi((data.substr(start, end)).c_str()));

	start = data.find("Number Humans:") + 14;
	end = data.find("\n", start);
	//Y->SETnumHumans(atoi((data.substr(start, end)).c_str()));
}

///Purpose: this function opens the file, reads the data, then closes it.
///Written By: Evan
bool Load::LoadfromFile(string file)
{
	fin.open(("../saves/"+file+".sav").c_str());
	getline(fin, data);
	fin.close();

	return dataExists();
}

///Purpose: to load the game's title given an input of the file name.
///Written By: Evan
wstring Load::LoadTitle(wstring title)
{
	//C File with a buffer of 200 characters on a single line
	FILE* TitleFile;
	wchar_t buffer[200];

	//Open the file given the name of the file passed into the function
	wstring file = L"../data/";
	file.append(title);
	file.append(L".dat"); //All loaded data comes from a .dat extension
	//_wfopen_s(&TitleFile, file.c_str(), L"r, ccs=UTF-16LE"); //The UTF-16LE indicates the title data is stored as Unicode and NOT ANSI

	title.erase();
	while(!feof(TitleFile))
	{
		//Getline function in C code
		fgetws(buffer, (sizeof(buffer) / sizeof(wchar_t)), TitleFile);

		//If the file is empty, break the loop (ie. corrupted file)
		if(wcslen(buffer)<5)
            break;

		title.append(buffer);
	}

	fclose(TitleFile);
	fflush(TitleFile);

	return title;
}

///Purpose:: to decrypt the data loaded from .sav file (game data)
///Written By: Evan
void Load::decode()
{
	//Decrypt the coded text and save to copy
	for (unsigned int i = 0; i < data.length(); i++) {
		srand(i);
		data[i] = data[i] - floor((sqrt(i + rand() % 100) + 1));
	}
}

///Purpose: to decrypt the data loaded from the .dat file given the title string
///Written By: Evan
void Load::decode(wstring& title)
{
	//Decrypt the coded text and save to copy
	for (unsigned int i = 0; i < title.length(); i++) {
		srand(i);
		title[i] = title[i] - floor((sqrt(i + rand() % 100) + 1));
	}
}


///Purpose: for decoding files that contain data in the upper limit of Unicode,
///the rand() value has been lowered to prevent going over the data stream
///Written By: Evan
void Load::decode2(wstring& title)
{
	//Decrypt the coded text and save to copy
	for (unsigned int i = 0; i < title.length(); i++) {
		srand(i);
		title[i] = title[i] - floor((sqrt(i + rand() % 20) + 1));
	}
}

///Written By: Evan
bool Load::dataExists()
{
	return !!data.length();
}

///Written By: Evan
bool Load::AskLoad()
{
	if(Display().AskToLoad())
		return true;
	return false;
}




















